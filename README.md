This tool allows you to automaticaly set BM user passwords when they come close to expiration

### Installation
run command: `npm install`

There are 2 script files that can be executed: `cypher-pass.js` and `index.js`.

### Password encrypt
`cypher-pass.js` is used for encryption and decryption of a password for a BM user. Later this password can be stored in `config.js`

In the command line type:

```javascript
node cypher-pass.js HASH PASSWORD encrypt
```

* HASH - the combination of letters and numbers that will be used for encryption - `Vxa3mA2beaQxn8tAmHXT`
* PASSWORD - BM user password

### Password decrypt
You can test your encrypted password or decrypt other passwords with
```javascript
node cypher-pass.js HASH ENCRYPTEDPASSWORD decrypt
```

* HASH - the combination of letters and numbers that will be used for encryption - `Vxa3mA2beaQxn8tAmHXT`
* ENCRYPTEDPASSWORD - encrypted password

Once encrypted, the password and user login have to be inserted in `config.js` like this

```javascript
"users": {
	"punchout2go": "a8a2828daebd5d52f154d0c7a676dbcd"
}
```

### Passwords information
Run the following command to get information for all listed in `config.js` users
```javascript
node index.js HASH EXPTIME getPasswordInfo
```

* HASH - the combination of letters and numbers that will be used for encryption - `Vxa3mA2beaQxn8tAmHXT`
* EXPTIME - days until password will expire
* getPasswordInfo - if this argument is added, tool will only log information about BM users listed in `config.js`

Projects and instances (dev, staging, production, sandbox) have to be updated in `config.js`.
For example:

```javascript
const PROJECTS = {
	"LSS": {
		"instances": {
			"https://dev02-eu01-lifesports.demandware.net": {
				"users": {
					"yuriytest": "10b923ada784fca001af90d938fcd02a"
				}
			}
		},
		"clientID": "f64804ed-a008-4aa6-a308-a791ec7ba42e",
		"secret": "z#e95rO2ybqdX&K"
	},
	"TTS": {
		"instances": {
			"https://development-shop-tts.demandware.net": {
				"users": {
					"punchout2go": "a8a2828daebd5d52f154d0c7a676dbcd"
				}
			}
		},
		"clientID": "22944793-e83f-4ca5-8161-4c8181fccfed",
		"secret": "s_^]';5scrCw4P7&"
	}
}
```

* clientID - OCAPI Client ID that will have the Data type permissions to access and manipulate BM users:
```javascript
	{
      "client_id": "22944793-e83f-4ca5-8161-4c8181fccfed",
      "resources": [
        {
          "resource_id": "/users/this",
          "methods": [
            "get"
          ],
          "read_attributes": "(**)",
          "write_attributes": "(**)"
        },
        {
          "resource_id": "/users/this/password",
          "methods": [
            "patch"
          ],
          "read_attributes": "(**)",
          "write_attributes": "(**)"
        }
      ]
    }
```
* secret - generated when client ID is created

In order to run and test the functionality type in the command line:

`node index.js HASH EXPTIME`

* HASH - the combination of letters and numbers that will be used for encryption - `Vxa3mA2beaQxn8tAmHXT`
* EXPTIME - days until password will expire

### OCAPI Settings
Following Data configuration should be set as minimal for selected API Client ID

```javascript
{
    "_v": "18.2",
    "clients": [{
        "client_id": "<API_CLIENT_ID>",
        "resources": [{
            "resource_id": "/users/this",
            "methods": ["get"],
            "read_attributes": "(**)",
            "write_attributes": "(**)"
        }, {
            "resource_id": "/users/this/password",
            "methods": ["patch"],
            "read_attributes": "(**)",
            "write_attributes": "(**)"
        }]
    }]
}
```

https://bitbucket.org/blue-leaf/bl-update-users/src/master/bitbucket-pipelines.yml
https://bitbucket.org/blue-leaf/bl-update-users/admin/addon/admin/pipelines/repository-variables