const request = require('request-promise');
var clc = require("cli-color");

const utils = require('./scripts/utils');
const CONFIG = require('./scripts/config');

const args = process.argv.slice(2);
const hash = args[0];
const EXP_TIME = args[1];
const PASS_CHECK = args[2];

let PROJECTS = CONFIG.PROJECTS;
let tempPass = null;
let isLastPass = false;
let shouldUpdatePass = false;
let passCheckTimes = 0; // No need to check more then one times

// Get Access Token for further OCAPI calls
let getAccessToken = async (params) => {
	tempPass = tempPass || params.password;
	const uri = `${params.domain}/dw/oauth2/access_token?client_id=${params.clientID}`;
	// Generate Authorization Bearer token
	const bearerToken = Buffer.from(params.username+':'+tempPass+':'+params.secret).toString('base64');

	let body = utils.buildRequestBody({
		uri: uri,
		method: 'POST',
		headers: {
			'content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
			'Authorization': `Basic ${bearerToken}`
		},
		form: {
			grant_type: 'urn:demandware:params:oauth:grant-type:client-id:dwsid:dwsecuretoken'
		}
	});

	return request(body)
		.then(async (data) => {
			params.access_token = data.access_token;
			await getUserStatus(params);
		})
		.catch((err) => {
			console.log(`Couldn't get access token for user ${clc.red(params.username)} on instance ${clc.yellow(params.domain)}: ${err}`);
		});
}

// Get Current User Status
let getUserStatus = async (params) => {
	const uri = `${params.domain}/s/-/dw/data/v18_1/users/this?client_id=${params.clientID}`;

	let body = utils.buildRequestBody({
		uri: uri,
		headers: {
			'Authorization': `Bearer ${params.access_token}`
		}
	});

	return request(body)
		.then(async (data) => {
			if (PASS_CHECK === 'getPasswordInfo') {
				if (passCheckTimes === 0) {
					utils.getPasswordInfo(data, params.domain, EXP_TIME);
				}
				passCheckTimes++;

				return;
			}
			if (utils.checkPassExpDate(data) < EXP_TIME || shouldUpdatePass === true) {
				shouldUpdatePass = true;
				await setUserPassword(params);
			} else {
				isLastPass = true;
				console.log(`Password for user ${clc.red(params.username)} on instance ${clc.yellow(params.domain)} doesn't have to be updated!`);

				return;
			}
		})
		.catch((err) => {
			console.log('getUserStatus: ' + err);
		});
}

let setUserPassword = async (params) => {
	const uri = `${params.domain}/s/-/dw/data/v18_1/users/this/password?client_id=${params.clientID}`;
	let i = '0';
	let pass = `${tempPass + i}`;

	if (isLastPass) {
		pass = params.password;
	}

	let body = utils.buildRequestBody({
		uri: uri,
		method: 'PATCH',
		headers: {
			'content-type': 'application/json;charset=UTF-8',
			'Authorization': `Bearer ${params.access_token}`
		},
		body: {
			"current_password" : `${tempPass}`,
			"password": pass
		},
		json: false
	});

	tempPass = tempPass + i;

	if (isLastPass) {
		tempPass = null;
		isLastPass = false;
		shouldUpdatePass = false;
	}

	return request(body)
		.then((data) => {
			console.log(`Password for user ${clc.green(params.username)} on instance ${clc.yellow(params.domain)} has been changed successfully!`);
		})
		.catch((err) => {
			console.log(`Couldn't change password for user ${clc.red(params.username)} on instance ${clc.yellow(params.domain)}: ${err}`);
		});
}

let updatePasswordsStart = async () => {
	for (let p in PROJECTS) {
		if (PROJECTS.hasOwnProperty(p)) {
			let secret = PROJECTS[p].secret;
			let clientID = PROJECTS[p].clientID;
			let instances = PROJECTS[p].instances;
			for (let instance in instances) {
				let users = instances[instance].users;
				for (let user in users) {
					let password = utils.decrypt(hash, users[user]);
					let params = {
						"secret": secret,
						"clientID": clientID,
						"domain": instance,
						"username": user,
						"password": password
					};

					// clear temp password, otherwise it's always first users password
					tempPass = null;

					for (i = 0; i < 5; i++) {
						if (i === 4) {
							isLastPass = true;
							passCheckTimes = 0;
						}

						await getAccessToken(params);
					}
				}
			}
		}
	}
}

updatePasswordsStart();
