// This file can be used by a user / developer to check , encrypt or decrypt passwords, because we save them encrypted
const utils = require('./scripts/utils');

const args = process.argv.slice(2);
const pass = args[1];
const hash = args[0];
const func = args[2];

if (func === 'encrypt') {
	console.log('--------If you encrypt---------');
	console.log(utils.encrypt(hash,pass));
} else {
	console.log('--------If you decrypt---------');
	console.log(utils.decrypt(hash,pass));
}
