var crypto = require('crypto');
var clc = require("cli-color");

var utils = (() => {
	/**
	 * Builds request body by given configuration
	 *
	 * @param {Object} options
	 * @returns {Object} body - Request body
	 */
	function buildRequestBody(options) {
		let body = {
			method: options.method || 'GET',
			uri: options.uri,
			headers: options.headers,
			form: options.form || '',
			body: options.body || '',
			json: options.json || true
		};
		return body;
	}

	// Check remaing days until password expires
	let checkPassExpDate = (data) => {
		let expDate = new Date(data.password_expiration_date);
		let today = new Date();
		let diffTime = expDate.getTime() - today.getTime();
		let diffDays = Math.ceil(diffTime / (1000 * 3600 * 24));

		return diffDays;
	}

	let getPasswordInfo = (data, instance, exp) => {
		let user = data.login;
		let modDate = new Date(data.password_modification_date).toLocaleString('en-GB');
		let expDate = new Date(data.password_expiration_date).toLocaleString('en-GB');
		let lastLoginDate = new Date(data.last_login_date).toLocaleString('en-GB').split(', ')[0];
		let shouldUpdate = (checkPassExpDate(data) < exp) ? clc.red('Yes') : clc.green('No');
		let info = `Password information for user ${clc.green(user)} on instance ${clc.yellow(instance)}\n\n`
			+ `Last login: ${lastLoginDate}\n`
			+ `Modified: ${modDate}\n`
			+ `Expiration: ${expDate}\n`
			+ `Days left: ${checkPassExpDate(data)}\n`
			+ `Should update: ${shouldUpdate}\n`;

		console.log(info);

		return;
	}

	let encrypt = (h,p) => {
	  var cipher = crypto.createCipher('aes-256-ecb', h);
	  return cipher.update(p, 'utf8', 'hex') + cipher.final('hex');
	}

	let decrypt = (h,p) => {
	  var cipher = crypto.createDecipher('aes-256-ecb', h);
	  return cipher.update(p, 'hex', 'utf8') + cipher.final('utf8');
	}

	return {
		buildRequestBody: buildRequestBody,
		checkPassExpDate: checkPassExpDate,
		getPasswordInfo: getPasswordInfo,
		encrypt: encrypt,
		decrypt: decrypt
	}
})();

module.exports = utils;